package dev.throwable.spec.beans;

import java.util.ArrayList;
import java.util.List;

public class ErrorMessages {

    private List<ErrorMessage> errors = new ArrayList<>();

    public List<ErrorMessage> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorMessage> errors) {
        this.errors = errors;
    }

}
