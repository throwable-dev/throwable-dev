package dev.throwable.spec.beans;

import java.util.Map;

public class ErrorMessage {

    private Integer status;

    private Integer code;

    private String title;

    private String detail;

    private Map<String, Object> source;

    public ErrorMessage() {

    }

    public ErrorMessage(Integer code, String title, String detail) {
        this.code = code;
        this.title = title;
        this.detail = detail;
    }

    public ErrorMessage(Integer status, Integer code, String title, String detail) {
        this(code, title, detail);
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Map<String, Object> getSource() {
        return source;
    }

    public void setSource(Map<String, Object> source) {
        this.source = source;
    }

}
