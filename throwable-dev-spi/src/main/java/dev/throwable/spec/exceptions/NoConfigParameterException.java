package dev.throwable.spec.exceptions;

public class NoConfigParameterException extends RuntimeException {

	private static final long serialVersionUID = -2564965279775631283L;
	
	private String parameterName;
	
	public NoConfigParameterException(String parameterName) {
		this.parameterName = parameterName;
	}

	public NoConfigParameterException(String message, Throwable cause) {
		super(message, cause);
	}

	public String getParameterName() {
		return parameterName;
	}
	
}
