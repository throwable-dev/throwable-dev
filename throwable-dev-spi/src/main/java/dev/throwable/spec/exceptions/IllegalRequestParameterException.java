package dev.throwable.spec.exceptions;

/**
 * Это исключение возникает в случае ошибки парсинга параметра.
 * 
 * @author Vitaly Masterov
 * @since 2.8
 *
 */
public class IllegalRequestParameterException extends Exception {

	private static final long serialVersionUID = -8996810676229500869L;
	
	private String parameter;
	
	public IllegalRequestParameterException(String parameter) {
		this.parameter = parameter;
	}

	public IllegalRequestParameterException(String parameter, Exception e) {
		super(e);
		this.parameter = parameter;
	}
	
	public String getParameter() {
		return parameter;
	}
	
}