package dev.throwable.spec.exceptions;

public class InvalidIdentifierFormatException extends RuntimeException {

	private static final long serialVersionUID = -3489335473352704323L;

	private String identifier;

	public InvalidIdentifierFormatException(String identifier) {
		this.identifier = identifier;
	}

	public String getIdentifier() {
		return identifier;
	}

}
