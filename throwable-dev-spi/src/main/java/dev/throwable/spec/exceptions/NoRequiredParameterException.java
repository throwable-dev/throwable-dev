package dev.throwable.spec.exceptions;

public class NoRequiredParameterException extends RuntimeException {

	private static final long serialVersionUID = 4853692006199949889L;
	
	private String label;
	
	public NoRequiredParameterException() {
		super();
	}
	
	public NoRequiredParameterException(String label) {
		super();
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
	
}