package dev.throwable.spec.exceptions;

/**
 * Исключение, выбрасываемое при отсутствии реализации
 */

public class NoQualifierException extends Exception {

	private static final long serialVersionUID = 4237740273935448088L;

    /**
     * Наименование класса
     */
    private String className;


    public <T> NoQualifierException(Class<T> classObject) {
         this.className = classObject.getCanonicalName();
    }

	public NoQualifierException() {
		super();
	}

	public NoQualifierException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public NoQualifierException(String message, Throwable cause) {
		super(message, cause);
	}

	public NoQualifierException(String message) {
		super(message);
	}

	public NoQualifierException(Throwable cause) {
		super(cause);
	}

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
