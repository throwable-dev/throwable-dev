package dev.throwable.spec.exceptions;

import java.util.Locale;

public class ThrowableDevException extends Exception {

    private String context;

    private Locale locale;

    public ThrowableDevException(Throwable cause, String context, Locale locale) {
        super(cause);
        this.context = context;
        this.locale = locale;
    }

    public ThrowableDevException(Throwable cause, Locale locale) {
        super(cause);
        this.locale = locale;
    }

    public String getcontext() {
        return context;
    }

    public Locale getLocale() {
        return locale;
    }

}
