package dev.throwable.spec.exceptions;

public class NoRequestParameterException extends Exception {

	private static final long serialVersionUID = -2564965279775631283L;
	
	private String parameterName;
	
	public NoRequestParameterException(String parameterName) {
		this.parameterName = parameterName;
	}

	public String getParameterName() {
		return parameterName;
	}
	
}
