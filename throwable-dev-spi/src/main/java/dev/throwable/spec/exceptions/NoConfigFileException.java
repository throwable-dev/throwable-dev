package dev.throwable.spec.exceptions;

public class NoConfigFileException extends RuntimeException {

	private static final long serialVersionUID = -9126702292819971974L;

	/**
	 * Указанное место расположения файла конфигурации
	 */
	private String location;

	public NoConfigFileException(String location) {
		this.location = location;
	}

	public String getLocation() {
		return location;
	}

}
