package dev.throwable.spec.exceptions;

public class ResourceNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 676388164098492464L;
	
	private String naturalId;

	public ResourceNotFoundException(String naturalId) {
		super();
		this.naturalId = naturalId;
	}

	public String getNaturalId() {
		return naturalId;
	}
	
}