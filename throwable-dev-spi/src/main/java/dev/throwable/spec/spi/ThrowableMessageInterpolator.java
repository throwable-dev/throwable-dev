package dev.throwable.spec.spi;

import java.util.ResourceBundle;

@FunctionalInterface
public interface ThrowableMessageInterpolator<T extends Throwable> {

    String DETAIL = ".detail";

    String interpolate(T throwable, ResourceBundle rBundle);

    default String getMessage(T throwable, ResourceBundle rBundle, Object... parameters) {
        return String.format(rBundle.getString(throwable.getClass().getName().concat(DETAIL)), parameters);
    }

    default String getMessage(T throwable, ResourceBundle rBundle) {
        return rBundle.getString(throwable.getClass().getName().concat(DETAIL));
    }

}
