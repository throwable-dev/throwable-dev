package dev.throwable.spec.spi;

public interface PropertyName {

    String getProperty();

}
