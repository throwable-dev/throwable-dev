package dev.throwable.spec.spi;

import java.lang.annotation.Annotation;
import java.util.Map;

public interface ConstraintCodeMapping {

    Map<Class<? extends Annotation>, Integer> getCodes();

}
