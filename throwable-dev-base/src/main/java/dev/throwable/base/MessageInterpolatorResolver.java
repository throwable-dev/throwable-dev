package dev.throwable.base;

import dev.throwable.spec.spi.ThrowableMessageInterpolator;

import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.ServiceLoader;

public class MessageInterpolatorResolver {

    private static MessageInterpolatorResolver instance;

    private Map<Class<? extends Throwable>, ? super ThrowableMessageInterpolator> interpolators = new HashMap<>();

    private MessageInterpolatorResolver() {
        Iterator<ThrowableMessageInterpolator> iter = ServiceLoader.load(ThrowableMessageInterpolator.class).iterator();
        while (iter.hasNext()) {
            ThrowableMessageInterpolator interpolator = iter.next();
            Class<? extends Throwable> exceptionClass = (Class<? extends Throwable>)((ParameterizedType)(interpolator.getClass().getGenericSuperclass())).getActualTypeArguments()[0];
            interpolators.put(exceptionClass, interpolator);
        }
    }

    public ThrowableMessageInterpolator resolve(Class<? extends Throwable> exceptionClass) {
        return (ThrowableMessageInterpolator)interpolators.get(exceptionClass);
    }

    public static MessageInterpolatorResolver getInstance() {
        if (instance == null) {
            instance = new MessageInterpolatorResolver();
        }

        return instance;
    }

}
