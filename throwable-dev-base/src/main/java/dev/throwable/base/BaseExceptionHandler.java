package dev.throwable.base;

import dev.throwable.spec.beans.ErrorMessage;
import dev.throwable.spec.beans.ErrorMessages;
import dev.throwable.spec.exceptions.ThrowableDevException;
import dev.throwable.spec.spi.PropertyName;

import java.util.Map;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import java.util.function.Function;

public class BaseExceptionHandler implements Function<ThrowableDevException, ErrorMessages> {

    public static final Integer DEFAULT_STATUS = 400;

    public static final Integer DEFAULT_CODE = 1002;

    public static final String DEFAULT_ERROR =  "java.lang.UnknownError";

    private static final String RESOURCE_BUNDLE_NAME = "ErrorMessages";

    private static final String COMMONS_RESOURCE_BUNDLE_NAME = "CommonsErrorMessages";

    public static final String TITLE = ".title";

    public static final String DETAIL = ".detail";

    public static final String PROPERTY = "property";

    private ErrorCodeMappingProvider errorCodeMappingProvider = ErrorCodeMappingProvider.getInstance();

    private CauseFinder causeFinder = new CauseFinder();

    @Override
    public ErrorMessages apply(ThrowableDevException exception) {
        ResourceBundle appRb = ResourceBundle.getBundle(RESOURCE_BUNDLE_NAME, exception.getLocale());
        ResourceBundle commonsRb = ResourceBundle.getBundle(COMMONS_RESOURCE_BUNDLE_NAME, exception.getLocale());

        ThrowableInstance instance = new ThrowableInstance(exception);
        causeFinder.accept(instance);
        Throwable throwable = instance.getThrowable();

        ErrorMessages errMsgs = new ErrorMessages();
        ErrorMessage errMsg = new ErrorMessage();
        errMsgs.getErrors().add(errMsg);

        Integer code = errorCodeMappingProvider.getCode(throwable.getClass());

        String exFqn = throwable.getClass().getName();
        ResourceBundle rb = appRb.containsKey(exFqn.concat(TITLE)) ? appRb : commonsRb;

        if (code != null) {
            errMsg.setStatus(errorCodeMappingProvider.getStatus(throwable.getClass()));
            errMsg.setCode(code);
            errMsg.setTitle(rb.getString(exFqn.concat(TITLE)));

            String detailMsg = rb.getString(exFqn.concat(DETAIL));
            if (detailMsg.contains("%")) {
                var msgInterpolator =  MessageInterpolatorResolver.getInstance().resolve(throwable.getClass());
                errMsg.setDetail(msgInterpolator.interpolate(throwable, rb));
            } else {
                errMsg.setDetail(detailMsg);
            }
            if (throwable instanceof PropertyName property) {
                errMsg.setSource(Map.of(PROPERTY, property.getProperty()));
//                errMsg.setStatus(409);
            }
        } else {
//            new StackTracePrinter().accept(exception);
            errMsg.setStatus(DEFAULT_STATUS);
            errMsg.setCode(DEFAULT_CODE);
            errMsg.setTitle(commonsRb.getString(DEFAULT_ERROR.concat(TITLE)));
            errMsg.setDetail(commonsRb.getString(DEFAULT_ERROR.concat(DETAIL)));
        }

        return errMsgs;
    }

    class CauseFinder implements Consumer<ThrowableInstance> {

        @Override
        public void accept(ThrowableInstance instance) {
            if (instance.getThrowable().getCause() != null) {
                instance.setThrowable(instance.getThrowable().getCause());
                accept(instance);
            }
        }

    }

    static final class ThrowableInstance {

        Throwable throwable;

        ThrowableInstance(Throwable throwable) {
            this.throwable = throwable;
        }

        public Throwable getThrowable() {
            return throwable;
        }

        public void setThrowable(Throwable throwable) {
            this.throwable = throwable;
        }

    }

}
