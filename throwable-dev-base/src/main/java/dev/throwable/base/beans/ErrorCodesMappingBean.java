package dev.throwable.base.beans;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ErrorCodesMappingBean {

    private List<ErrorCodeMappingItemBean> errors;

    @JsonProperty("errors")
    public List<ErrorCodeMappingItemBean> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorCodeMappingItemBean> errors) {
        this.errors = errors;
    }

}
