package dev.throwable.base.beans;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorCodeMappingItemBean {

    /**
     * Full-qualified class name
     */
    private String fqn;

    /**
     * Match http-status
     */
    private Integer status;

    /**
     * Math code from classifier
     */
    private Integer code;

    @JsonProperty("class")
    public String getFqn() {
        return fqn;
    }

    public void setFqn(String fqn) {
        this.fqn = fqn;
    }

    @JsonProperty("status")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @JsonProperty("code")
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

}
