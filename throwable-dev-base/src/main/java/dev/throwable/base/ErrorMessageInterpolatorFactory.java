package dev.throwable.base;

import dev.throwable.spec.spi.ThrowableMessageInterpolator;
import dev.throwable.spec.exceptions.IllegalRequestParameterException;
import dev.throwable.spec.exceptions.NoConfigParameterException;
import dev.throwable.spec.exceptions.NoRequestParameterException;
import dev.throwable.spec.exceptions.ResourceNotFoundException;

import java.util.ResourceBundle;

/**
 * Factory обработчиков, формирующих сообщения об ошибках, содержащих параметры.
 */
public class ErrorMessageInterpolatorFactory {

    public static class NoRequestParameterMessageInterpolator implements ThrowableMessageInterpolator<NoRequestParameterException> {

        @Override
        public String interpolate(NoRequestParameterException throwable, ResourceBundle rBundle) {
            return getMessage(throwable, rBundle, throwable.getParameterName());
        }

    }

    public static class IllegalRequestParameterMessageInterpolator implements ThrowableMessageInterpolator<IllegalRequestParameterException> {

        @Override
        public String interpolate(IllegalRequestParameterException throwable, ResourceBundle rBundle) {
            return getMessage(throwable, rBundle, throwable.getParameter());
        }

    }

    public static class NoConfigParameterMessageInterpolator implements ThrowableMessageInterpolator<NoConfigParameterException> {

        @Override
        public String interpolate(NoConfigParameterException throwable, ResourceBundle rBundle) {
            return getMessage(throwable, rBundle, throwable.getParameterName());
        }

    }

    public static class ResourceNotFoundMessageInterpolator implements ThrowableMessageInterpolator<ResourceNotFoundException> {

        @Override
        public String interpolate(ResourceNotFoundException throwable, ResourceBundle rBundle) {
            return getMessage(throwable, rBundle, throwable.getNaturalId());
        }

    }

}
