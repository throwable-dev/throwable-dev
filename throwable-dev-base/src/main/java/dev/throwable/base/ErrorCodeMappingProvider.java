package dev.throwable.base;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import dev.throwable.base.beans.ErrorCodeMappingItemBean;
import dev.throwable.base.beans.ErrorCodesMappingBean;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class ErrorCodeMappingProvider {

    private Map<Class<? extends Throwable>, Integer> codes;

    private Map<Class<? extends Throwable>, Integer> statuses;

    private static ErrorCodeMappingProvider instance;

    private ErrorCodeMappingProvider() {
        codes = new HashMap<>();
        statuses = new HashMap<>();
        new CommonMappingFileReader().run();
        new ApplicationMappingFileReader().run();
    }

    public static ErrorCodeMappingProvider getInstance() {
        if (instance == null) {
            instance = new ErrorCodeMappingProvider();
        }

        return instance;
    }

    class CommonMappingFileReader implements Runnable {

        @Override
        public void run() {
            ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            try (InputStream is = this.getClass().getResourceAsStream("/commons-errors.yaml")) {
                ErrorCodesMappingBean codesMapping = mapper.readValue(is, ErrorCodesMappingBean.class);
                for (ErrorCodeMappingItemBean itemBean : codesMapping.getErrors()) {
                    try {
                        Class throwableClass = Class.forName(itemBean.getFqn());
                        codes.put(throwableClass, itemBean.getCode());
                        statuses.put(throwableClass, itemBean.getStatus());
                    } catch(ClassNotFoundException e) {
                        // skip this exception
                        // Это означает, что в заданном модуле(микросервисе) отсутствует класс, описанный в errors.yaml
                    }
                }
            } catch(IOException e) {
                throw new RuntimeException(e);
            }
        }

    }

    class ApplicationMappingFileReader implements Runnable {

        @Override
        public void run() {
            ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            try (InputStream is = this.getClass().getResourceAsStream("/errors.yaml")) {
                ErrorCodesMappingBean codesMapping = mapper.readValue(is, ErrorCodesMappingBean.class);
                for (ErrorCodeMappingItemBean itemBean : codesMapping.getErrors()) {
                    try {
                        Class throwableClass = Class.forName(itemBean.getFqn());
                        codes.put(throwableClass, itemBean.getCode());
                        statuses.put(throwableClass, itemBean.getStatus());
                    } catch (ClassNotFoundException e) {
                        // skip this exception
                        // Это означает, что в заданном модуле(микросервисе) отсутствует класс, описанный в errors.yaml
                    }
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /*
    class CommonMappingFileReader implements Runnable {

        @Override
        public void run() {
            Path cfgFilePath = Paths.get(configsDir, "errors.yaml");
            if (Files.notExists(cfgFilePath)) {
                return;
            }
            ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            try {
                ErrorCodesMappingBean codesMapping = mapper.readValue(cfgFilePath.toFile(), ErrorCodesMappingBean.class);
                for (ErrorCodeMappingItemBean itemBean : codesMapping.getErrors()) {
                    try {
                        Class throwableClass = Class.forName(itemBean.getFqn());
                        codes.put(throwableClass, itemBean.getCode());
                        statuses.put(throwableClass, itemBean.getStatus());
                    } catch(ClassNotFoundException e) {
                        // skip this exception
                        // Это означает, что в заданном модуле(микросервисе) отсутствует класс, описанный в errors.yaml
                    }
                }
            } catch(IOException e) {
                throw new RuntimeException(e);
            }
        }

    }

    class ApplicationMappingFileReader implements Runnable {

        @Override
        public void run() {
            Path cfgFilePath = Paths.get(configsDir, appName.concat("-errors.yaml"));
            if (Files.notExists(cfgFilePath)) {
                return;
            }
            ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            try {
                ErrorCodesMappingBean codesMapping = mapper.readValue(cfgFilePath.toFile(), ErrorCodesMappingBean.class);
                for (ErrorCodeMappingItemBean itemBean : codesMapping.getErrors()) {
                    try {
                        Class throwableClass = Class.forName(itemBean.getFqn());
                        codes.put(throwableClass, itemBean.getCode());
                        statuses.put(throwableClass, itemBean.getStatus());
                    } catch(ClassNotFoundException e) {
                        // skip this exception
                        // Это означает, что в заданном модуле(микросервисе) отсутствует класс, описанный в errors.yaml
                    }
                }
            } catch(IOException e) {
                throw new RuntimeException(e);
            }
        }

    }
    */

    public Integer getCode(Class<? extends Throwable> throwableClass) {
        return codes.get(throwableClass);
    }

    public Integer getStatus(Class<? extends Throwable> throwableClass) {
        return statuses.get(throwableClass);
    }

}
