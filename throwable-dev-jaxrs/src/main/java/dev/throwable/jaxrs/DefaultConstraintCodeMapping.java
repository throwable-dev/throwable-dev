package dev.throwable.jaxrs;

import dev.throwable.spec.spi.ConstraintCodeMapping;
import jakarta.enterprise.context.Dependent;

import java.lang.annotation.Annotation;
import java.util.Map;

@Dependent
public class DefaultConstraintCodeMapping implements ConstraintCodeMapping {

    @Override
    public Map<Class<? extends Annotation>, Integer> getCodes() {
        return Map.of();
    }

}
