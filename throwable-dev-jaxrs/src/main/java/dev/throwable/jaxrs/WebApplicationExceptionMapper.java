package dev.throwable.jaxrs;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.throwable.base.BaseExceptionHandler;
import dev.throwable.spec.beans.ErrorMessages;
import dev.throwable.spec.exceptions.ThrowableDevException;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import java.util.Locale;
import java.util.function.Consumer;

/**
 * Маппер-обработки исключений типа WebApplicationException.
 * На самом деле в WebApplicationException должно быть завёрнуто
 * действительное исключение.
 *
 * @author Vitaly Masterov
 * @since 0.1
 * @see ExceptionMapper
 * @see WebApplicationException
 *
 */
@Provider
public class WebApplicationExceptionMapper implements ExceptionMapper<WebApplicationException> {

    private static final String DEFAULT_LANGUAGE = "en";

    private static final String ACCEPT_LANGUAGE = "Accept-Language";

    public static final String JSON_UTF8 = "application/json;UTF-8";

    @Context
    HttpHeaders httpHeaders;

    private BaseExceptionHandler baseExceptionHandler = new BaseExceptionHandler();

    @Override
    public Response toResponse(WebApplicationException exception) {
        String acceptLang = httpHeaders.getHeaderString(ACCEPT_LANGUAGE);
        if (acceptLang == null || acceptLang.equals("")) {
            acceptLang = DEFAULT_LANGUAGE;
        }
        String acceptLanguage = Locale.LanguageRange.parse(acceptLang).get(0).getRange().substring(0, 2);
        Locale locale = new Locale(acceptLanguage);
        ErrorMessages errMsgs = baseExceptionHandler.apply(new ThrowableDevException(exception.getCause(), locale));
        int httpStatus = errMsgs.getErrors().get(0).getStatus().intValue();

        ObjectMapper mapper = new ObjectMapper();
        try {
            return Response
                    .status(httpStatus)
                    .type(JSON_UTF8)
                    .entity(mapper.writeValueAsString(errMsgs))
                    .build();
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    class StackTracePrinter implements Consumer<Throwable> {
        @Override
        public void accept(Throwable exception) {
            System.out.println("===================================================");
            exception.printStackTrace();
            System.out.println("===================================================");
        }

    }

}
