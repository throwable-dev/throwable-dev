package dev.throwable.jaxrs;

import dev.throwable.spec.annotations.MessageTitle;
import dev.throwable.spec.beans.ErrorMessage;
import dev.throwable.spec.beans.ErrorMessages;
import dev.throwable.spec.spi.ConstraintCodeMapping;
import jakarta.inject.Inject;
import jakarta.validation.*;
import jakarta.validation.metadata.ConstraintDescriptor;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import org.hibernate.validator.engine.HibernateConstraintViolation;

import java.lang.annotation.Annotation;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.function.BiFunction;

@Provider
public class ConstraintViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {

    private static final String RESOURCE_BUNDLE_NAME = "org.hibernate.validator.ValidationMessages";

    private static final String ACCEPT_LANGUAGE = "Accept-Language";

    private static final String PROPERTY = "property";

    private static final String DEFAULT_LANGUAGE = "ru";

    private static final String TITLE_TPL = "{%s.title}";

    private static final String DETAIL_TPL = "{%s.detail}";

    @Context
    HttpHeaders httpHeaders;

    @Inject
    ConstraintCodeMapping constraintCodeMapping;

    @Inject
    ValidatorFactory validatorFactory;

    @Override
    public Response toResponse(ConstraintViolationException exception) {
        String acceptLang = httpHeaders.getHeaderString(ACCEPT_LANGUAGE);
        if (acceptLang == null || acceptLang.equals("")) {
            acceptLang = DEFAULT_LANGUAGE;
        }
        String acceptLanguage = Locale.LanguageRange.parse(acceptLang).get(0).getRange().substring(0, 2);
        Locale locale = new Locale(acceptLanguage);

        ErrorMessages errMsgs = new ErrorMessages();
        Iterator<ConstraintViolation<?>> iter = exception.getConstraintViolations().iterator();

        while (iter.hasNext()) {
            ConstraintViolation<?> cv = iter.next();
            if (cv.getConstraintDescriptor().isReportAsSingleViolation()) {
                var appErrMsg = new AppErrorMessageBuilder().apply(cv, locale);
                errMsgs.getErrors().add(appErrMsg);
                break;
            }
            Path.Node node = cv.getPropertyPath().iterator().next();

            ErrorMessage errMsg = new ErrorMessage();
            errMsg.setSource(Map.of(PROPERTY, node.getName()));

            errMsg.setStatus(409);
//            errMsg.setCode(code);

            String msgKey = cv.getMessageTemplate().substring(1, cv.getMessageTemplate().length()-1);
            MessageInterpolator msgInterpolator =  validatorFactory.getMessageInterpolator();

            MessageInterpolator.Context ctx = new MessageInterpolator.Context() {
                @Override
                public ConstraintDescriptor<?> getConstraintDescriptor() {
                    return cv.getConstraintDescriptor();
                }

                @Override
                public Object getValidatedValue() {
                    return cv.getInvalidValue();
                }

                @Override
                public <T> T unwrap(Class<T> type) {
                    return null;
                }

            };

            if (msgKey.startsWith("jakarta") || msgKey.startsWith("javax") || msgKey.startsWith("org.hibernate")) {
                errMsg.setTitle(msgInterpolator.interpolate(cv.getMessageTemplate(), ctx, locale));
            } else {
                errMsg.setTitle(msgInterpolator.interpolate(String.format(TITLE_TPL, msgKey), ctx, locale));
                errMsg.setDetail(msgInterpolator.interpolate(String.format(DETAIL_TPL, msgKey), ctx, locale));
            }

            errMsgs.getErrors().add(errMsg);
        }

        return Response
                .serverError()
                .status(Response.Status.CONFLICT)
                .type(MediaType.APPLICATION_JSON)
                .entity(errMsgs)
                .build();

    }

    class AppErrorMessageBuilder implements BiFunction<ConstraintViolation, Locale, ErrorMessage> {

        @Override
        public ErrorMessage apply(ConstraintViolation cv, Locale locale) {
            Annotation ann = cv.getConstraintDescriptor().getAnnotation();
            String title, detail;
            ResourceBundle rb = ResourceBundle.getBundle(RESOURCE_BUNDLE_NAME, locale);
            if (ann.annotationType().isAnnotationPresent(MessageTitle.class)) {
                String msgTpl = ann.annotationType().getAnnotation(MessageTitle.class).value();
                title = rb.getString(msgTpl.substring(1, msgTpl.length()-1));
            } else {
                title = "Error !";
            }
            detail = rb.getString(cv.getMessageTemplate().substring(1, cv.getMessageTemplate().length()-1));

            Integer code = constraintCodeMapping.getCodes().get(ann.annotationType());
            if (code == null) {
                code = 0;
            }

            HibernateConstraintViolation hcv = (HibernateConstraintViolation)cv.unwrap(HibernateConstraintViolation.class);
            Map payload = (Map)hcv.getDynamicPayload(Map.class);

            ErrorMessage errMsg = new ErrorMessage();
            errMsg.setStatus(409);
            errMsg.setCode(code);
            errMsg.setTitle(title);
            errMsg.setDetail(detail);
            errMsg.setSource(payload);

            return errMsg;
        }

    }

}
